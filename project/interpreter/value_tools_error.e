note
	description: "Summary description for {VALUE_TOOLS_ERROR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VALUE_TOOLS_ERROR

inherit
	RYTHME_ERROR
		redefine
			clear, exist
		end

feature -- Access

	clear
			-- <Precursor>
		do
			Precursor {RYTHME_ERROR}
			expression_not_valid := False
			numeric_not_valid := False
			boolean_not_valid := False
			cannot_compare := False
		end

	exist:BOOLEAN
			-- <Precursor>
		do
			Result :=
				Precursor {RYTHME_ERROR} or
				expression_not_valid or
				numeric_not_valid or
				boolean_not_valid or
				cannot_compare
		end

	copy_from(a_other:VALUE_TOOLS_ERROR)
			-- Copy every error from `a_other' into `Current'
		do
			line := a_other.line
			column := a_other.column
			erroneous_value := a_other.erroneous_value
			expression_not_valid := a_other.expression_not_valid
			numeric_not_valid := a_other.numeric_not_valid
			boolean_not_valid := a_other.boolean_not_valid
			cannot_compare := a_other.cannot_compare
			internal_error := a_other.internal_error
			syntax_not_valid := a_other.syntax_not_valid
		end

	expression_not_valid:BOOLEAN
			-- `Current' represent an invalid expression in the
			-- source code

	set_expression_not_valid
			-- Set `Current' as an invalid expression.
		do
			expression_not_valid := True
		end

	numeric_not_valid:BOOLEAN
			-- `Current' represent an error in a numeric value

	set_numeric_not_valid_with_value(a_value:READABLE_STRING_GENERAL)
			-- set `Current' as an error in the recognition of a numeric value.
			-- The invalid numeric value is represented by `a_value'
		do
			erroneous_value := a_value
			set_numeric_not_valid
		end

	set_numeric_not_valid
			-- set `Current' as an error in the recognition of a numeric value.
		do
			numeric_not_valid := True
		end

	boolean_not_valid:BOOLEAN
			-- `Current' represent an error in the recognition of a boolean value

	set_boolean_not_valid_with_value(a_value:READABLE_STRING_GENERAL)
			-- set `Current' as an error in the recognition of a boolean value.
			-- The invalid boolean value is represented by `a_value'
		do
			erroneous_value := a_value
			boolean_not_valid := True
		end

	set_boolean_not_valid
			-- set `Current' as an error in the recognition of a boolean value.
		do
			boolean_not_valid := True
		end

	cannot_compare:BOOLEAN
			-- `Current' represent an error when attempting to compare two objects
			-- that cannot be compare.

	set_cannot_compare_with_value(a_values:READABLE_STRING_GENERAL)
			-- Set `Current' as an error when attempting to compare two objects
			-- that cannot be compare. The two objects are represented by `a_values'
		do
			erroneous_value := a_values
			cannot_compare := True
		end



end
