note
	description: "Interpreter error system."
	author: "Louis Marchand"
	date: "2014, november 10"
	revision: "0.1"

class
	INTERPRETER_ERROR

inherit
	VALUE_TOOLS_ERROR
		rename
			set_syntax_not_valid as set_syntax_not_valid_with_position
		redefine
			clear, exist
		end
	RYTHME_NODE_ERROR
		rename
			set_syntax_not_valid as set_syntax_not_valid_with_position
		redefine
			clear, exist
		end

feature -- Access

	clear
			-- <Precursor>
		do
			Precursor {VALUE_TOOLS_ERROR}
			variable_not_found := False
			function_not_found := False
			list_index_not_valid := False
			operator_used_on_void_variable := False
			has_custom_error := False
			has_function_name_duplicate_error := False
			has_argument_count_error := False
			has_list_not_valid := False
			has_module_not_found := False
			has_function_used_in_procedure_call := False
			has_module_argument_count_not_valid := False
			has_file_not_valid := False
			has_file_path_not_valid := False
			has_command_name_not_valid := False
			has_arguments_not_valid := False
		end

	exist:BOOLEAN
			-- <Precursor>
		do
			Result :=
				Precursor {VALUE_TOOLS_ERROR} or
				variable_not_found or
				function_not_found or
				list_index_not_valid or
				operator_used_on_void_variable or
				has_custom_error or
				has_function_name_duplicate_error or
				has_argument_count_error or
				has_list_not_valid or
				has_module_not_found or
				has_function_used_in_procedure_call or
				has_module_argument_count_not_valid or
				has_file_not_valid or
				has_file_path_not_valid or
				has_command_name_not_valid or
				has_arguments_not_valid
		end
		

	set_expression_not_valid_with_node(a_node:RYTHME_AST_NODE)
			-- Set `Current' as an invalid expression.
			-- The error happend in `a_node'
		do
			set_line_and_column(a_node)
			expression_not_valid := True
		end

	variable_not_found:BOOLEAN
			-- `Current' represent an error in a variable ID.

	set_variable_not_found(a_node:RYTHME_AST_NODE)
			-- Set `Current' as representing an error in  a variable ID.
			-- The error happend in `a_node'
		do
			set_line_and_column(a_node)
			variable_not_found := True
		end

	set_variable_not_found_with_value(a_node:RYTHME_AST_NODE; a_value:READABLE_STRING_GENERAL)
			-- Set `Current' as representing an error in  a variable ID.
			-- The error happend in `a_node' and the invalid ID is representing
			-- by `a_value'
		do
			erroneous_value := a_value
			set_line_and_column(a_node)
			variable_not_found := True
		end

	function_not_found:BOOLEAN
			-- `Current' represent an error in a function ID.

	set_function_not_found(a_node:RYTHME_AST_NODE)
			-- Set `Current' as representing an error in  a function ID.
			-- The error happend in `a_node'
		do
			set_line_and_column(a_node)
			function_not_found := True
		end

	set_function_not_found_with_value(a_node:RYTHME_AST_NODE; a_value:READABLE_STRING_GENERAL)
			-- Set `Current' as representing an error in  a function ID.
			-- The error happend in `a_node' and the invalid ID is representing
			-- by `a_value'
		do
			erroneous_value := a_value
			set_line_and_column(a_node)
			function_not_found := True
		end

	list_index_not_valid:BOOLEAN
			-- `Current' represent a list index error.

	set_list_index_not_valid_with_value(a_node:RYTHME_AST_NODE; a_value:READABLE_STRING_GENERAL)
			-- Set `Current' as representing a list index error.
			-- The error happend in `a_node' and the invalid index is
			-- representing by `a_value'
		do
			erroneous_value := a_value
			set_line_and_column(a_node)
			list_index_not_valid := True
		end

	set_numeric_not_valid_with_node(a_node:RYTHME_AST_NODE)
			-- set `Current' as an error in the recognition of a numeric value.
			-- The error happend in `a_node'.
		do
			set_line_and_column (a_node)
			set_numeric_not_valid
		end

	operator_used_on_void_variable:BOOLEAN
			-- `Current' represent an error when attempting to use an operation
			-- on a Void Variable

	set_operator_used_on_void_variable(a_node:RYTHME_AST_NODE)
			-- Set `Current' as an error when attempting to to use an operation
			-- on a Void Variable. The error happend in `a_node'.
		do
			set_line_and_column (a_node)
			operator_used_on_void_variable := True
		end

	set_operator_used_on_void_variable_with_token(a_token:RYTHME_TOKEN)
			-- Set `Current' as an error when attempting to to use an operation
			-- on a Void Variable. The error happend with the operator in `a_token'.
		do
			line := a_token.line
			column := a_token.column
			operator_used_on_void_variable := True
		end

	has_argument_count_error:BOOLEAN
			-- `Current' represent an error when using a module with an invalid argument count.

	set_argument_count_error
			-- Set `Current' an error when using a module with an invalid argument count.
		do
			has_argument_count_error := True
		end

	has_function_name_duplicate_error:BOOLEAN
			-- `Current' represent an error when two function in the system has the same name.

	set_function_name_duplicate_error(a_name: READABLE_STRING_GENERAL)
			-- Set `Current' an error when two function in the system has the same name.
			-- `erroneous_value' is set to `a_name'
		do
			has_function_name_duplicate_error := True
			erroneous_value := a_name
		end

	has_list_not_valid:BOOLEAN
			-- `Current' represent an error when trying to index a variable that is not a list

	set_list_not_valid(a_node:RYTHME_AST_NODE; a_name:READABLE_STRING_GENERAL)
			-- Set `Current' an error when trying to index a variable that is not a list.
			-- The error happend in `a_node'. `erroneous_value' is set to `a_name'.
		do
			has_list_not_valid := True
			set_line_and_column (a_node)
			erroneous_value := a_name
		end

	has_module_not_found:BOOLEAN
			-- `Current' represent an error when trying to use a module and it does not exist

	set_module_not_found(a_node:RYTHME_AST_NODE; a_name:READABLE_STRING_GENERAL)
			-- Set `Current' an error when trying to use a module and it does not exist.
			-- The error happend in `a_node'. `erroneous_value' is set to `a_name'.
		do
			has_module_not_found := True
			set_line_and_column (a_node)
			erroneous_value := a_name
		end

	has_function_used_in_procedure_call:BOOLEAN
			-- `Current' represent an error when trying to use a function module in a procedure call

	set_function_used_in_procedure_call(a_node:RYTHME_AST_NODE; a_name:READABLE_STRING_GENERAL)
			-- Set `Current' an error when trying to use a function module in a procedure call.
			-- The error happend in `a_node'. `erroneous_value' is set to `a_name'.
		do
			has_function_used_in_procedure_call := True
			set_line_and_column (a_node)
			erroneous_value := a_name
		end

	has_module_argument_count_not_valid:BOOLEAN
			-- `Current' represent an error when trying to use a module with a bad number of argument

	set_module_argument_count_not_valid(a_node:RYTHME_AST_NODE)
			-- Set `Current' an error when trying to use a module with a bad number of argument.
			-- The error happend in `a_node'.
		do
			has_module_argument_count_not_valid := True
			set_line_and_column (a_node)
		end

	has_file_not_valid:BOOLEAN
			-- `Current' represent an error when use a file that is not valid

	set_file_not_valid(a_node:RYTHME_AST_NODE)
			-- Set `Current' an error when trying to use  a file that is not valid
			-- The error happend in `a_node'.
		do
			has_file_not_valid := True
			set_line_and_column (a_node)
		end

	has_file_path_not_valid:BOOLEAN
			-- `Current' represent an error when use a file path that is not valid

	set_file_path_not_valid
			-- Set `Current' an error when trying to use  a file path that is not valid
			-- The error happend in `a_node'.
		do
			has_file_path_not_valid := True
		end

	has_command_name_not_valid:BOOLEAN
			-- `Current' represent an error when use a command name that is not valid

	set_command_name_not_valid(a_node:RYTHME_AST_NODE; a_name:READABLE_STRING_GENERAL)
			-- Set `Current' an error when trying to use a command name that is not valid.
			-- `a_name' is the name of the invalid command. The error happend in `a_node'.
		do
			has_command_name_not_valid := True
			set_line_and_column (a_node)
			erroneous_value := a_name
		end

	has_arguments_not_valid:BOOLEAN
			-- `Current' represent an error when use a command or function with an invalid argument

	set_arguments_not_valid
			-- Set `Current' an error when trying to use a command or function with an invalid argument
		do
			has_arguments_not_valid := True
		end

	has_custom_error:BOOLEAN
			-- `Current' represent a custom error defined by the `erroneous_value' text

	set_custom_error(a_message: READABLE_STRING_GENERAL)
			-- Set `Current' as an custom error. `erroneous_value' is set to `a_message'
		do
			has_custom_error := True
			erroneous_value := a_message
		end

end
