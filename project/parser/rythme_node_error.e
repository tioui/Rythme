note
	description: "Summary description for {RYTHME_NODE_ERROR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	RYTHME_NODE_ERROR

inherit
	RYTHME_ERROR

feature -- Access

	set_internal_error_with_node(a_node:RYTHME_AST_NODE)
			-- Set `Current' as an internal error.
			-- The error happend in `a_node'.
			-- Should never happend! This is a bug!
		do
			set_line_and_column(a_node)
			internal_error := True
		end

	set_syntax_not_valid_with_node(a_node:RYTHME_AST_NODE)
			-- set `Current' as an error in the code syntax
			-- The error happend in `a_node'
		do
			set_line_and_column(a_node)
			syntax_not_valid := True
		end

	set_line_and_column(a_node:RYTHME_AST_NODE)
			-- Assign `line' and `column' from associated values in `a_node'
		do
			source := a_node.source
			line:=a_node.line
			if attached  a_node.token as la_token then
				column := la_token.column
			else
				column := 0
			end
		end

end
