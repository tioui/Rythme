note
	description: "Skeleton (Base method and attributes) for the Rythme scanner."
	author: "Louis Marchand"
	date: "2014, October 13"
	revision: "0.2"

deferred class
	RYTHME_SCANNER_SKELETON

inherit
	YY_COMPRESSED_SCANNER_SKELETON
		rename
			make_with_file as yy_make_with_file
		undefine
			default_create
		redefine
			yy_make_with_file,
			fatal_error
		end

	RYTHME_TOKENS
		undefine
			default_create
		end

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current' using the standard input as source input
		do
			make

		end

	yy_make_with_file (a_file: KI_CHARACTER_INPUT_STREAM)
			-- Initialization of `Current' using `a_file' as source input
		do
			Precursor {YY_COMPRESSED_SCANNER_SKELETON}(a_file)
			source := a_file.name
			create error
		end

	make_with_file_name (a_file_name: READABLE_STRING_GENERAL)
			-- Initialization of `Current' using `a_file_name' to open the source input file
		local
			l_file:KL_TEXT_INPUT_FILE
		do
			create l_file.make (a_file_name.to_string_8)
			l_file.open_read
			yy_make_with_file(l_file)
		end

	make_with_string(a_string:READABLE_STRING_GENERAL)
			-- Initialization of `Current' using `a_string' as source input
		do
			yy_make_with_file(create {KL_STRING_INPUT_STREAM}.make(a_string.to_string_8))
		end

feature -- Access

	source:READABLE_STRING_GENERAL
			-- The source code (file name or index) that `Current' is scanning


	error:RYTHME_SCANNER_ERROR
			-- The error manager

	has_error:BOOLEAN
			-- Is `Current' in error state
		do
			Result := error.exist
		end

feature {NONE} -- Implementation


	add_token(a_code:INTEGER)
			-- A token has been found. Add it to the abstract syntax tree
		local
			l_token:RYTHME_TOKEN
		do
			create l_token.make (a_code, column, text_count, line, text, source)
			create last_detachable_rythme_ast_node_value.make_from_token (l_token)
			last_token := a_code
		end

	fatal_error(a_message:STRING_8)
			-- Set an internal error when trigger.
			-- Should never happend.
		do
			error.set_syntax_not_valid (line, column)
			error.set_source (source)
		end

end
