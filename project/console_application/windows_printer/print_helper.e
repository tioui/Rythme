note
	description: "Wrap the ISO print for Windows console"
	author: "Louis Marchand"
	date: "2014, november 6"
	revision: "1.0"

deferred class
	PRINT_HELPER

feature {NONE} -- Access

	print_standard(a_message:READABLE_STRING_GENERAL)
			-- Standard output method
		do
			io.standard_default.put_string (a_message.to_string_8)
		end

	print_error(a_message:READABLE_STRING_GENERAL)
			-- Error output method
		do
			io.error.put_string (a_message.to_string_8)
		end

	converter:UTF_CONVERTER
			-- The Eiffel {STRING_GENERAL} to UTF converter
		once
			create Result
		end

end
