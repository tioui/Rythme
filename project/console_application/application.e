note
	description : "Rythme console interpreter"
	date        : "2014, october 10"
	revision    : "0.2"

class
	APPLICATION

inherit
	ARGUMENTS
	PRINT_HELPER

create
	make

feature {NONE} -- Initialization


	make
			-- Run application.
		local
			l_constants:CONSTANTS
		do
			create {CONSTANTS_FRENCH}l_constants
			if argument_count < 1 then
				print_error (l_constants.error_usage_prefix + command_name + l_constants.error_usage_sufix)
				io.put_new_line
			else
				if attached parse_files(argument_array.subarray (1, argument_count), l_constants) as la_ast then
					if across la_ast.children as la_children some la_children.key.is_case_insensitive_equal ("main") end then
						launch_interpreter(la_ast, l_constants)
					else
						print_error (l_constants.error_main_module_not_found)
					end
				end
			end
		end

	launch_interpreter(a_ast:RYTHME_AST_NODE; a_constants:CONSTANTS)
			-- Launch the interpreter on the abstract syntaxic node `a_ast'
			-- and using the locals in `a_constants'
		local
			l_interpreter:RYTHME_INTERPRETER
			l_default_functions_factory: DEFAULT_COMMANDS_FACTORY
		do
			if attached a_ast.children.at ("main") as la_main then
				create l_interpreter.make_with_constants (la_main, extract_modules(a_ast), a_constants)
				l_interpreter.set_print_agent (agent print_standard )
				create l_default_functions_factory.make_with_constants (a_constants)
				l_interpreter.add_functions (l_default_functions_factory.functions)
				l_interpreter.add_commands (l_default_functions_factory.procedure)
				if not l_interpreter.has_error then
					l_interpreter.run
				end
				if l_interpreter.has_error then
					constants_interpreter_error(l_interpreter.error, a_constants)
					highlight_error (l_interpreter.error)
				end
			else
				print_error (a_constants.error_general + "%N")
				print_error (a_constants.error_no_main_module + "%N")
			end
		end

	extract_modules(a_ast:RYTHME_AST_NODE):STRING_TABLE[RYTHME_AST_NODE]
			-- Every children of `a_ast' that represent a module
		do
			create Result.make (a_ast.children.count)
			across a_ast.children as la_list loop
				if
					attached la_list.item as la_item and then
					(la_item.is_function or la_item.is_procedure)
				then
					Result.extend (la_item, la_list.key)
				end
			end
		end

	parse_files(a_file_names:ITERABLE[READABLE_STRING_GENERAL]; a_constants:CONSTANTS):detachable RYTHME_AST_NODE
			-- Launch the Rythme parser on the file `a_file_name' using the locals in `a_constants'
			-- and return the abstract syntactic tree of the program
		local
			l_parser: RYTHME_FRENCH_PARSER
			l_file_iterator:ITERATION_CURSOR[READABLE_STRING_GENERAL]
			l_has_error:BOOLEAN
			l_file_validator:RAW_FILE
		do
			from
				l_file_iterator := a_file_names.new_cursor
				l_has_error := False
			until
				l_file_iterator.after or
				l_has_error
			loop
				create l_file_validator.make_with_name (l_file_iterator.item)
				if l_file_validator.exists and then l_file_validator.is_readable then
					create l_parser.make_with_file_name (l_file_iterator.item)
					if attached Result then
						l_parser.set_ast (Result)
					end
					l_parser.parse
					if l_parser.is_accepted then
						if not attached Result then
							Result := l_parser.ast
						end
					else
						Result := Void
						l_has_error := True
						if l_parser.has_scanner_error then
							constants_scanner_error (l_parser.scanner_error, a_constants)
							highlight_error(l_parser.scanner_error)
						elseif l_parser.has_parser_error then
							constants_parser_error (l_parser.parser_error, a_constants)
							highlight_error(l_parser.parser_error)
						end
					end
				else
					Result := Void
					l_has_error := True
					print_error (a_constants.error_source_file_not_valid + l_file_iterator.item + "%N")
				end
				l_file_iterator.forth
			end

		end

	highlight_error(a_error:RYTHME_ERROR)
			-- Show the line text of `a_error' and
			-- Highlight the surounding of the error with an arrow
		local
			l_file:PLAIN_TEXT_FILE
			i:INTEGER
		do
			if attached a_error.source as la_source then
				if a_error.line > 0 then
					create l_file.make_open_read (la_source)
					from i := 1 until
						i > a_error.line or
						l_file.end_of_file
					loop
						l_file.read_line
						i:=i+1
					end
					if not l_file.end_of_file then
						print_error (string_no_tab(l_file.last_string) + "%N")
					end
					if a_error.column > 0 then
						from i := 1 until i > a_error.column - 1 loop
							print_error ("-")
							i:=i+1
						end
						print_error ("^%N")
					end
				end
			end

		end

	string_no_tab(a_string:READABLE_STRING_GENERAL):READABLE_STRING_GENERAL
			-- Copy of `a_string' with no tab character in it (replaced by space)
		local
			l_result :STRING_GENERAL
			i:INTEGER
		do
			l_result := a_string.as_string_32
			from i := 1 until i > l_result.count loop
				if l_result[i] ~ '%T' then
					l_result.put_code ((' ').code.as_natural_32, i)
				end
				i :=i + 1
			end
			Result := l_result
		end

	general_error(a_error:RYTHME_ERROR; a_constants:CONSTANTS)
			-- The common method to call before almost every error manager method
		do
			if attached a_error.source as la_source then
				print_error (a_constants.error_general_in_file)
				print_error (la_source)
			else
				print_error (a_constants.error_general)
			end
			print_error (" (")
			print_error (a_constants.line_text + ": " + a_error.line.out)
			print_error (", " + a_constants.column_text.to_string_8 + ": " + a_error.column.out)
			print_error (")%N")
		end

	constants_scanner_error(a_error:RYTHME_SCANNER_ERROR; a_constants:CONSTANTS)
			-- Print the error string from `a_constant' matching the error in `a_error'
		do
			general_error (a_error, a_constants)
			if a_error.internal_error then
				print_error(a_constants.error_internal_error+"%N")
			elseif a_error.syntax_not_valid then
				print_error(a_constants.error_syntax_not_valid+"%N")
			elseif a_error.numeric_not_valid then
				print_error (a_constants.error_numeric_not_valid+"%N")
			end
			if attached a_error.erroneous_value as la_value then
				print_error (a_constants.error_incorrect_value + ": " + la_value + "%N")
			end
		end

	constants_parser_error(a_error:RYTHME_PARSER_ERROR; a_constants:CONSTANTS)
			-- Print the error string from `a_constant' matching the error in `a_error'
		do
			general_error (a_error, a_constants)
			if a_error.internal_error then
				print_error(a_constants.error_internal_error+"%N")
			elseif a_error.syntax_not_valid then
				print_error(a_constants.error_syntax_not_valid+"%N")
			elseif a_error.module_name_not_valid then
				print_error (a_constants.error_module_name_not_valid+"%N")
			elseif a_error.module_name_already_in_use then
				print_error (a_constants.error_module_name_already_in_use+"%N")
			elseif a_error.main_module_duplicated then
				print_error (a_constants.error_main_module_duplicated+"%N")
			end
			if attached a_error.erroneous_value as la_value then
				print_error (a_constants.error_incorrect_value + ": " + la_value + "%N")
			end
		end

	constants_interpreter_error(a_error:INTERPRETER_ERROR; a_constants:CONSTANTS)
			-- Print the error string from `a_constant' matching the error in `a_error'
		do
			general_error (a_error, a_constants)
			if a_error.internal_error then
				print_error(a_constants.error_internal_error+"%N")
			elseif a_error.syntax_not_valid then
				print_error(a_constants.error_syntax_not_valid+"%N")
			elseif a_error.expression_not_valid then
				print_error (a_constants.error_expression_not_valid+"%N")
			elseif a_error.variable_not_found then
				print_error (a_constants.error_variable_not_found+"%N")
			elseif a_error.function_not_found then
				print_error (a_constants.error_function_not_found+"%N")
			elseif a_error.numeric_not_valid then
				print_error (a_constants.error_numeric_not_valid+"%N")
			elseif a_error.boolean_not_valid then
				print_error (a_constants.error_boolean_not_valid+"%N")
			elseif a_error.cannot_compare then
				print_error (a_constants.error_cannot_compare+"%N")
			elseif a_error.list_index_not_valid then
				print_error (a_constants.error_list_index_not_valid+"%N")
			elseif a_error.operator_used_on_void_variable then
				print_error (a_constants.error_operator_used_on_void_variable+"%N")
			elseif a_error.has_argument_count_error then
				print_error (a_constants.error_argument_count+"%N")
			elseif a_error.has_function_name_duplicate_error then
				print_error (a_constants.error_function_name_duplicate+"%N")
			elseif a_error.has_list_not_valid then
				print_error (a_constants.error_list_not_valid+"%N")
			elseif a_error.has_module_not_found then
				print_error (a_constants.error_module_not_found+"%N")
			elseif a_error.has_function_used_in_procedure_call then
				print_error (a_constants.error_function_used_in_procedure_call+"%N")
			elseif a_error.has_module_argument_count_not_valid then
				print_error (a_constants.error_module_argument_count_not_valid+"%N")
			elseif a_error.has_file_not_valid then
				print_error (a_constants.error_file_not_valid+"%N")
			elseif a_error.has_file_path_not_valid then
				print_error (a_constants.error_file_path_not_valid+"%N")
			elseif a_error.has_command_name_not_valid then
				print_error (a_constants.error_command_name_not_valid+"%N")
			elseif a_error.has_arguments_not_valid then
				print_error (a_constants.error_arguments_not_valid+"%N")
			end
			if attached a_error.erroneous_value as la_value then
				if a_error.has_custom_error then
					print_error (la_value + "%N")
				else
					print_error (a_constants.error_incorrect_value + ": " + la_value + "%N")
				end

			end
		end



end
